<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        // [["color" => [item_variations]], ["size" => [item_variations]]]
        $vars = array_values($this->variations()->get()->groupBy('id')->map(function($item, $key) {
            return [\App\Variation::find($key)->name => $item->all()] ;
        })->all());
        // transform vars into [["name"=>"", "item_variations"=>[]]]
        array_map(function($i) use (&$vars) {
            $item_variations = array_values($vars[$i])[0];
            $name = array_keys($vars[$i])[0];
            $id = $item_variations[0]->pivot->variation_id;
            unset($vars[$i][$name]);
            $vars[$i]["id"] = $id;
            $vars[$i]["name"] = $name;
            $vars[$i]["item_variations"] = array_map(function($item) {
                return [
                    "id" => $item->pivot->id,
                    "value" => $item->pivot->value, 
                    "attachment" => $item->pivot->attachment];
            }, $item_variations);
        }, array_keys($vars));

        return [
            'name' => $this->name,
            'description' => $this->detail,
            'price' => $this->price,
            'stock' => $this->stock == 0 ? 'Out of Stock' : $this->stock,
            'discount' =>$this->discount,
            'totalPrice' => round(( 1 - ($this->discount/100)) * $this->price,2),
            'rating' => $this->reviews->count() > 0 ? round($this->reviews->sum('star')/$this->reviews->count(),2) : 'No rating yet',
            'href' => [
                'reviews' => route('reviews.index',$this->id)
            ],
            'imageUrl' => $this->imageUrl,
            'category' => $this->category,
            'label' => $this->label,
            'variations' => $vars
        ];
    }
}
