<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\Resource;

class ItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'stock' => 'Out of Stock',
            'discount' =>$this->discount_price,
            'totalPrice' => round(( 1 - ($this->discount_price/100)) * $this->price,2),
            'rating' => 'No rating yet',
            'href' => [],
            'imageUrl' => $this->image,
            'category' => $this->category,
            'label' => $this->label,
            'variations' => $this->variations()
        ];
    }
}
