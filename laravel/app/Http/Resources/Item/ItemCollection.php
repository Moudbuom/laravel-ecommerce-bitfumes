<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\Resource;

class ItemCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->title,
            'discount' =>$this->discount_price,
            'totalPrice' => round(( 1 - ($this->discount_price/100)) * $this->price,2),
            'rating' => 'No rating yet',
            'href' => [],
            'imageUrl' => $this->image,
            'category' => $this->category,
            'label' => $this->label,
        ];
    }
}
