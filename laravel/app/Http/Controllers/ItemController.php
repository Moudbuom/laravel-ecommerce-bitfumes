<?php

namespace App\Http\Controllers;


use App\Exceptions\ItemNotBelongsToUser;
use App\Http\Requests\ItemRequest;
use App\Http\Resources\Item\ItemCollection;
use App\Http\Resources\Item\ItemResource;
use App\Model\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemCollection::collection(Item::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(ItemRequest $request)
    // {
    //     $item = new Item;
    //     $item->name = $request->name;
    //     $item->detail = $request->description;
    //     $item->stock = $request->stock;
    //     $item->price = $request->price;
    //     $item->discount = $request->discount;
    //     $item->save();
    //     return response([
    //         'data' => new ProductResource($product)
    //     ],Response::HTTP_CREATED);
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return new ItemResource($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        // $this->ProductUserCheck($product);
        // $request['detail'] = $request->description;
        // unset($request['description']);
        // $product->update($request->all());
        // return response([
        //     'data' => new ProductResource($product)
        // ],Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        // $this->ProductUserCheck($product);
        // $product->delete();
        // return response(null,Response::HTTP_NO_CONTENT);
    }

    // public function ProductUserCheck($product)
    // {
    //     if (Auth::id() !== $product->user_id) {
    //         throw new ProductNotBelongsToUser;
    //     }
    // }
}
