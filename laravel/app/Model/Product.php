<?php

namespace App\Model;

use App\Model\Review;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'name','detail','stock','price','discount', 'imageUrl', 'category', 'label'
	];
    public function reviews()
    {
    	return $this->hasMany(Review::class); 
		}
	public function variations() 
	{
		return $this->belongsToMany(\App\Variation::class)->withPivot('id', 'value', 'attachment');
	}
}
