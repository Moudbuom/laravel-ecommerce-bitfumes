<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Corvariation;


class ItemCorvariation extends Model
{
    //
    protected $table = 'core_itemvariation';
    public $timestamps = false;

    private static function formItemVariations($variation_id) {
        // Get item_variations from $k
        $filtered = ItemCorvariation::all()->filter(function($v) use ($variation_id) {
            return $v->variation_id == $variation_id;
        });
        return $filtered->map(function($v) {
            return [
                'id' => $v->id,
                'value' => $v->value,
                'attachment' => $v->attachment
            ];
        })->all();
    }

    public static function getVariationsForItem($item_id) {
        $var_ids = Corvariation::getVariationsForItem($item_id);

        $vars = [];
        foreach($var_ids as $k => $v) {
            $elem = [
                'name' => $v, 
                'item_variations'=>ItemCorvariation::formItemVariations($k)
            ];
            array_push($vars, $elem);
        }
        return $vars;
    }
}
