<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ItemCorvariation;

class Item extends Model
{
    protected $table = 'core_item';
    //
    public $timestamps = false;
    public function variations() {
        return ItemCorvariation::getVariationsForItem($this->id);
    }
}
