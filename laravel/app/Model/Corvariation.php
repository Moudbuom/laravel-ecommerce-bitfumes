<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Corvariation extends Model
{
    //
    protected $table = 'core_variation';
    public $timestamps = false;

    public static function getVariationsForItem($item_id) {
        // return [ variation_id => 'Color']
        $vars_for_item = Corvariation::all()->filter(
            function($v) use ($item_id) {
                return $v->item_id == $item_id;
            });
        $vars = [];
        $vars_for_item->map(function($v) use(&$vars) {
            return $vars[$v->id] =  $v->name;
        });
        return $vars;
    }
}
