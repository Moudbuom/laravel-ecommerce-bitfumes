<?php

namespace App\Exceptions;

use Exception;

class ItemNotBelongsToUser extends Exception
{
    public function render()
    {
    	return ['errors' => 'Item Not Belongs to User'];
    }
}
