<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function() {
    Route::apiResource('/products','ProductController');
    Route::apiResource('/items','ItemController');
});

Route::group(['prefix'=>'products', 'middleware' => 'cors'],function(){
	Route::apiResource('/{product}/reviews','ReviewController');
});
