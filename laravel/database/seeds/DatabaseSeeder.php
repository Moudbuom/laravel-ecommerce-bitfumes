<?php

use Illuminate\Database\Seeder;

const VAR_COLOR = 1;
const VAR_SIZE = 2;
const VAR_VOLT = 3;

const VAR_COLOR_VALS = ['red', 'blue', 'white', 'black', 'green'];
const VAR_SIZE_VALS = ['XS', 'S', 'M', 'L', 'XL', 'XXL'];
const VAR_VOLT_VALS = [110, 220];


function RetreiveRandElem($elems) {
    return ($elems [rand(0, sizeof($elems)-1)]);
}

function PopulateItemVariationElems($var_vals, $count) {
    $faker = Faker\Factory::create();
    shuffle($var_vals);
    $v = array_map(
        function($item) use ($faker) {
            return [
                'value' => $item, 
                'attachment'=> $faker->imageUrl($width = 640, $height = 480),
            ];
        },
        array_slice($var_vals, 0, $count)
    );
    return ($v);
}

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class,5)->create();
        $products = factory(App\Model\Product::class,50)->create();
        factory(App\Model\Review::class,300)->create();
        $this->populateVariation($products);
    }

    private function populateVariation($products) {
        $products->each(function (App\Model\Product $p) {
            if(rand(1,10) <= 5) {
                $count = rand(1, sizeof(VAR_COLOR_VALS));
                foreach(PopulateItemVariationElems(VAR_COLOR_VALS, $count) as $varItem) {
                    $p->variations()->attach([
                        VAR_COLOR=>$varItem,
                    ]);
                }
                $count = rand(1, sizeof(VAR_SIZE_VALS));
                foreach(PopulateItemVariationElems(VAR_SIZE_VALS, $count) as $varItem) {
                    $p->variations()->attach([VAR_SIZE=>$varItem]);
                }
            } else {
                $count = rand(1, sizeof(VAR_VOLT_VALS));
                foreach(PopulateItemVariationElems(VAR_VOLT_VALS, $count) as $varItem) {
                    $p->variations()->attach([VAR_VOLT=>$varItem]);
                }
            }
        });
    }
}
